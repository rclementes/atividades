<?php 

class Atividade extends CI_Model
{

    public function todas($status = null, $situacao = null) {
        if($status) {
            $this->db->where('status', $status);
        }
        if($situacao) {
            $situacao = $situacao == 'T' ? 1 : 0;
            $this->db->where('situacao', $situacao);
        }
        $this->db->from('atividades AS AT');
        $this->db->join('status AS ST', 'AT.status = ST.statusID');
        $this->db->select('AT.*, ST.nome AS nome_status');
        return $this->db->get()->result();
    }

    public function porID($atividadeID) {
        return $this->db->where('atividadeID', $atividadeID)->get('atividades')->row();
    }

    public function inserir($dados) {
        $this->db->insert('atividades', $dados);
    }

    public function modificar($dados, $id) {
        $this->db->where('atividadeID', $id);
        $this->db->update('atividades', $dados);
    }


}