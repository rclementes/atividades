<?php $this->load->view('includes/_begin'); ?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">&nbsp;</div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <?php if(validation_errors()) {?>
                    <div class="alert alert-danger" role="alert">
                        <?php echo validation_errors(); ?>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="well">
                    <h4>Editar atividade: <?= $atividade->nome ?></h4>
                    <?= form_open('atividades/modificar');?>
                    <input type="hidden" name="atividadeID" value="<?= $atividade->atividadeID?>">
                    <div class="row">
                        <div class="form-group col-lg-12 col-md-12 col-sm-12">
                            <label>Nome</label>
                            <input type="text" class="form-control" name="nome" value="<?= $atividade->nome ?>" placeholder="Nome" required maxlength="255" <?= $atividade->status == 4 ? 'disabled' : ''?>>
                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12">
                            <label>Descrição</label>
                            <textarea class="form-control" name="descricao" placeholder="Descrição" maxlength="600" <?= $atividade->status == 4 ? 'disabled' : ''?>><?= $atividade->descricao ?></textarea>
                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12">
                            <label>Data Inicio</label>
                            <input type="date" class="form-control" name="data_inicio" value="<?= $atividade->data_inicio ?>" required <?= $atividade->status == 4 ? 'disabled' : ''?>>
                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12">
                            <label>Data Fim</label>
                            <input type="date" class="form-control" name="data_fim" value="<?= $atividade->data_fim ?>" <?= $atividade->status == 4 ? 'required' : ''?> <?= $atividade->status == 4 ? 'disabled' : ''?>>
                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12">
                            <label>Status</label>
                            <select class="form-control" name="status" <?= $atividade->status == 4 ? 'disabled' : ''?>>
                                <?php foreach($status as $item): ?>
                                    <option value="<?= $item->statusID?>" <?= $item->statusID == $atividade->status ? 'selected="selected"' : ''?>><?= $item->nome?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12">
                            <label>Situação</label>
                            <select class="form-control" name="situacao" <?= $atividade->status == 4 ? 'disabled' : ''?>>
                                <option value="1" <?= $atividade->situacao == 1 ? 'selected="selected"' : ''?>>Ativo</option>
                                <option value="0" <?= $atividade->situacao == 0 ? 'selected="selected"' : ''?>>Inativo</option>
                            </select>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <?php if($atividade->status != 4) :?>
                                <button class="btn btn-success" type="submit" role="button">Salvar</button>
                            <?php endif; ?>
                            <a href="atividades" class="btn btn-default">Voltar</a>
                        </div>
                    </div>
                    <?= form_close(); ?>
                </div>
            </div>
        </div>
    </div>


    <script>
        $(document).ready(function() {
            $('[name="status"]').on('change', function() {
                if($(this).val() == 4) {
                    $('[name="data_fim"]').prop('required',true);
                } else {
                    $('[name="data_fim"]').prop('required',false);
                }
            })
        })
    </script>







<?php $this->load->view('includes/_end'); ?>