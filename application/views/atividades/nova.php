<?php $this->load->view('includes/_begin'); ?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">&nbsp;</div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <?php if(validation_errors()) {?>
                    <div class="alert alert-danger" role="alert">
                        <?php echo validation_errors(); ?>
                    </div>
                <?php } ?>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="well">
                    <h4>Incluir atividade</h4>
                    <?= form_open('atividades/cadastrar');?>
                    <div class="row">
                        <div class="form-group col-lg-12 col-md-12 col-sm-12">
                            <label>Nome</label>
                            <input type="text" class="form-control" name="nome" placeholder="Nome" required maxlength="255">
                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12">
                            <label>Descrição</label>
                            <textarea class="form-control" name="descricao" placeholder="Descrição" maxlength="600"></textarea>
                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12">
                            <label>Data Inicio</label>
                            <input type="date" class="form-control" name="data_inicio" required>
                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12">
                            <label>Data Fim</label>
                            <input type="date" class="form-control" name="data_fim">
                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12">
                            <label>Status</label>
                            <select class="form-control" name="status">
                                <?php foreach($status as $item): ?>
                                    <option value="<?= $item->statusID?>"><?= $item->nome?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12">
                            <label>Situação</label>
                            <select class="form-control" name="situacao">
                                <option value="1">Ativo</option>
                                <option value="0">Inativo</option>
                            </select>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <button class="btn btn-success" type="submit" role="button">Salvar</button>
                            <a href="atividades" class="btn btn-default">Voltar</a>
                        </div>
                    </div>
                    <?= form_close(); ?>
                </div>
            </div>
        </div>
    </div>


    <script>
        $(document).ready(function() {
            $('[name="status"]').on('change', function() {
                if($(this).val() == 4) {
                    $('[name="data_fim"]').prop('required',true);
                } else {
                    $('[name="data_fim"]').prop('required',false);
                }
            })
        })
    </script>







<?php $this->load->view('includes/_end'); ?>