<?php $this->load->view('includes/_begin'); ?>
<?php
$message = $this->session->flashdata('success');
if($message) {
    ?>
    <div class="alert alert-success" role="alert">
        <?= $message ?>
    </div>
<?php } ?>

    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">&nbsp;</div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <form method="get" class="col-lg-12 col-md-12 col-sm-12">
                <div class="well">
                    <div class="row">
                        <div class="form-group col-lg-6 col-md-6 col-sm-12">
                            <label>Situação</label>
                            <select class="form-control" name="situacao">
                                <option value="">Selecione</option>
                                <option value="<?= base64_encode('T')?>" <?= $filtro['situacao'] == 'T' ? 'selected' : ''?>>Ativo</option>
                                <option value="<?= base64_encode('F')?>" <?= $filtro['situacao'] == 'F' ? 'selected' : ''?>>Inativo</option>
                            </select>
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-12">
                            <label>Status</label>
                            <select class="form-control" name="status">
                                <option value="">Selecione</option>
                                <?php foreach($status as $item): ?>
                                    <option value="<?= base64_encode($item->statusID) ?>" <?= $filtro['status'] == $item->statusID ? 'selected' : ''?>><?= $item->nome?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-12 col-md-12 col-sm-12">
                            <button type="submit" class="btn btn-default" role="button">Filtrar</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <br/>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="well">
                    <h4>Atividades</h4>
                    <table class="table table-condensed" id="example">
                        <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Descrição Abreviada</th>
                            <th>Data Inicio</th>
                            <th>Data Fim</th>
                            <th>Status</th>
                            <th>Situação</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if(count($atividades) <= 0) {?>
                            <tr>
                                <td colspan="7" class="text-center">Você não cadastrou nenhuma atividade!</td>
                            </tr>
                            <?php
                        } else {
                            foreach($atividades as $atividade):
                                ?>
                                <tr class="<?= $atividade->status == 4 ? 'success' : ''?>">
                                    <td><?= $atividade->nome?></td>
                                    <td><?= substr($atividade->descricao, 0, 20) . (strlen($atividade->descricao) > 20 ? '...' : '')?></td>
                                    <td><?= dateIsoToDatePt($atividade->data_inicio)?></td>
                                    <td><?= $atividade->data_fim != '0000-00-00' ? dateIsoToDatePt($atividade->data_fim) : ''?></td>
                                    <td><?= $atividade->nome_status?></td>
                                    <td><?= $atividade->situacao == 1 ? 'Ativo' : 'Inativo'?></td>
                                    <td>
                                        <a class="btn btn-primary" href="atividades/detalhes/<?= base64_encode($atividade->atividadeID)?>" role="button">Ver</a>
                                    </td>
                                </tr>
                                <?php
                            endforeach;
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <a class="btn btn-success" href="atividades/nova" role="button">Nova atividade</a>
            </div>
        </div>
    </div>
<?php $this->load->view('includes/_end'); ?>