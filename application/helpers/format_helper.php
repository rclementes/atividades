<?php


function dateIsoToDatePt($dateIso) {
    if($dateIso) {
        $date = explode('-', $dateIso);
        return $date[2] . '/' . $date[1] . '/' . $date[0];
    }
}
