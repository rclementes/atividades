<?php
session_save_path(APPPATH . 'session');

class Atividades extends CI_Controller
{
    public $viewData;

    public function __construct() {
        parent::__construct();
        $this->load->model('status');
        $this->load->model('atividade');
        $this->viewData['status'] = Status::todos();
    }

    public function index() {
        $status = isset($_GET['status']) ? base64_decode($_GET['status']) : null;
        $situacao = isset($_GET['situacao']) ? base64_decode($_GET['situacao']) : null;
        $atividadeInstance = new Atividade();
        $this->viewData['filtro'] = array(
            'status' => $status,
            'situacao' => $situacao,
        );
        $this->viewData['atividades'] = $atividadeInstance->todas($status, $situacao);
        $this->viewData['jsonData'] = json_encode($atividadeInstance->todas($status, $situacao));
        $this->load->view('atividades/lista', $this->viewData);
    }

    public function nova() {
        $this->load->view('atividades/nova', $this->viewData);
    }

    public function detalhes($atividadeID) {
        $atividade = new Atividade();
        $this->viewData['atividade'] = $atividade->porID(base64_decode($atividadeID));
        $this->load->view('atividades/editar', $this->viewData);
    }

    public function cadastrar() {
        if(!$this->validate()) {
            redirect('atividades/nova');
        }
        $atividade = new Atividade();
        $post = $this->input->post();
        $atividade->inserir($post);
        $this->session->set_flashdata('success', 'Atividade cadastrada com sucesso!');
        redirect('atividades');
    }

    public function modificar() {
        if(!$this->validate()) {
            redirect('atividades/nova');
        }
        $atividade = new Atividade();
        $post = $this->input->post();
        $atividade->modificar($post, $post['atividadeID']);
        $this->session->set_flashdata('success', 'Atividade modificada com sucesso!');
        redirect('atividades');
    }

    private function validate() {
        $config = array(
            array(
                'field' => 'nome',
                'label' => 'Nome',
                'rules' => 'required|max_length[255]',
                'errors' => array(
                    'required' => 'O campo nome é obrigatório',
                    'max_length' => 'O campo nome pode conter apenas 255 caracteres',
                ),
            ),
            array(
                'field' => 'descricao',
                'label' => 'Descrição',
                'rules' => 'required|max_length[600]',
                'errors' => array(
                    'required' => 'O campo descrição é obrigatório',
                    'max_length' => 'O campo descrição pode conter apenas 600 caracteres',
                ),
            ),
            array(
                'field' => 'data_inicio',
                'label' => 'Data de Inicio',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'O campo data de inicio é obrigatório',
                ),
            ),
        );
        $this->form_validation->set_rules($config);
        return $this->form_validation->run();
    }


}
