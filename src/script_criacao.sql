SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `atividades` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `atividades` ;

-- -----------------------------------------------------
-- Table `atividades`.`status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `atividades`.`status` (
  `statusID` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) NULL,
  PRIMARY KEY (`statusID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `atividades`.`atividades`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `atividades`.`atividades` (
  `atividadeID` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(255) NOT NULL,
  `descricao` VARCHAR(600) NOT NULL,
  `data_inicio` DATE NOT NULL,
  `data_fim` DATE NULL,
  `status` INT NULL,
  `situacao` BINARY NULL DEFAULT 1,
  PRIMARY KEY (`atividadeID`),
  INDEX `fk_status_idx` (`status` ASC),
  CONSTRAINT `fk_status`
    FOREIGN KEY (`status`)
    REFERENCES `atividades`.`status` (`statusID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

INSERT INTO `atividades`.`status` (`nome`) VALUES ('Pendente');
INSERT INTO `atividades`.`status` (`nome`) VALUES ('Em Desenvolvimento');
INSERT INTO `atividades`.`status` (`nome`) VALUES ('Em Teste');
INSERT INTO `atividades`.`status` (`nome`) VALUES ('Concluído');



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


